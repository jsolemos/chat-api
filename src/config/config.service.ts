import { JwtModuleOptions, JwtOptionsFactory } from "@nestjs/jwt";
import { TypeOrmOptionsFactory } from "@nestjs/typeorm";
import * as dotenv from "dotenv";
import * as fs from "fs";
import * as path from "path";
import { ConnectionOptions } from "typeorm";

export class ConfigService implements JwtOptionsFactory, TypeOrmOptionsFactory {
  private readonly envConfig: { [key: string]: string };

  get extensions() {
    console.log({ config: this.envConfig.NODE_ENV });
    return this.envConfig.NODE_ENV !== "local" ? "js" : "ts";
  }

  get distFolder() {
    return this.envConfig.NODE_ENV !== "local" ? "dist" : "src";
  }

  constructor(filePath: string) {
    console.log({ filePath });
    try {
      this.envConfig = Object.assign(
        dotenv.parse(fs.readFileSync(filePath)),
        process.env,
      );
    } catch (error) {
      this.envConfig = process.env;
    }
    this.populateEnv();
  }
  populateEnv() {
    for (const env in this.envConfig) {
      process.env[env] = process.env[env] || this.envConfig[env];
    }
  }

  get(key: string): string {
    return this.envConfig[key];
  }
  /**
   * DB Config
   */
  async createTypeOrmOptions(
    connectionName?: string,
  ): Promise<ConnectionOptions> | Promise<ConnectionOptions> {
    const config: ConnectionOptions = {
      type: "postgres",
      host: this.envConfig["DB_HOST"] || "localhost",
      port: parseInt(this.envConfig["DB_PORT"]) || 5432,
      username: this.envConfig["DB_USERNAME"] || "postgres",
      password: this.envConfig["DB_PASSWORD"] || "",
      database: this.envConfig["DB_DATABASE"],
      migrations: [`${this.distFolder}/migrations/*.${this.extensions}`],
      entities: [path.join(`${this.distFolder}/**/*.entity.${this.extensions}`)],
      logging: Boolean(this.envConfig["DB_LOGGING"]) || false,
      migrationsRun: false,
      dropSchema: false,
      synchronize: false,
      cli: {
        migrationsDir: `${this.distFolder}/migrations`,
      }
    };

    const ormConf = [
      config,
      {
        ...config,
        name: "populate",
        migrations: [`${this.distFolder}/populate/*.${this.extensions}`],
        migrationsTableName: "populate",
        cli: {
          migrationsDir: `${this.distFolder}/populate`,
        }
      }
    ];

    try {
      // Cria arquivo de config para typeorm CLI
      fs.writeFileSync(
        path.join("ormconfig.json"),
        JSON.stringify(ormConf, null, 2),
      );
    } catch (error) {
      console.error(error);
    }
    console.log({ config });
    return config;
  }

  /**
   * JWT Config
   */
  createJwtOptions(): JwtModuleOptions {
    return {
      secretOrPrivateKey: this.envConfig["JWT_SECRET"],
      signOptions: {
        expiresIn: this.envConfig["JWT_EXPIRES_IN"] || "1h", // padrão 1h
      },
    };
  }

  getPublicRoutes() {
    return ["/check", "/auth/login", "/clients", "/card/api-docs/"];
  }
}
