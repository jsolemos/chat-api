import { Module } from "@nestjs/common";
import * as path from "path";
import { ConfigService } from "./config.service";
@Module({
  providers: [
    {
      provide: ConfigService,
      useValue: new ConfigService(path.resolve(`${process.env.NODE_ENV || "local"}.env`)),
    },
  ],
  exports: [ConfigService],
})
export class ConfigModule { }
