import { ArgumentsHost, Catch, ExceptionFilter, HttpException, Inject } from "@nestjs/common";
import { Logger } from "../modules/logger/logger.service";

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(@Inject("Logger") private readonly logger: Logger) { }

  catch(exception: HttpException, host: ArgumentsHost) {
    console.error(">>>>>>>>>>>>> ERRORR");
    const ctx = host.switchToHttp();
    const res = ctx.getResponse();
    const req = ctx.getRequest();
    const status = exception.getStatus();
    res.status(status).json({
      ...exception.message,
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: req.originalUrl,
    });
  }
}
