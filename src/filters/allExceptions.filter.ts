import { ArgumentsHost, Catch, ExceptionFilter, HttpException, Inject } from "@nestjs/common";
import { isFunction } from "util";
import { Logger } from "../modules/logger/logger.service";

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(@Inject("Logger") private readonly logger: Logger) { }
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse();
    const req = ctx.getRequest();
    const status = isFunction(exception.getStatus) ? exception.getStatus() : "500";
    console.error(exception);
    res.status(status).json({
      ...(exception instanceof HttpException ? exception.message : exception),
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: req.originalUrl,
    });
  }
}
