import { BadRequestException } from "@nestjs/common";

export class BusinessException extends BadRequestException {
  constructor(message?: string | object | any, error?: string) {
    super(message, error);
    this.name = "Business Error";
  }
}