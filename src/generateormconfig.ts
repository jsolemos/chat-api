import * as path from "path";
import { ConfigService } from "./config/config.service";

new ConfigService(path.resolve(`${process.env.NODE_ENV || "local"}.env`)).createTypeOrmOptions();