import { createParamDecorator } from "@nestjs/common";

export const AuthClient = createParamDecorator((data, req) => {
  return req["authClient"];
});