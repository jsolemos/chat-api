import * as crypto from "crypto";
import * as proxy from "http-proxy-middleware";
import * as jwt from "jsonwebtoken";
import * as uuid from "uuid";

export function randomDouble() {
  return crypto.randomBytes(4).readUInt32LE(0) / 2 ** 32;
}

export function jwtVerify(token, secret) {
  return jwt.verify(token, secret);
}

export function hashPassword(password: string) {
  const salt = crypto.randomBytes(16).toString("hex");
  const hash = crypto.pbkdf2Sync(password, salt, 2048, 32, "sha512").toString("hex");
  return [salt, hash].join("$@");
}

export function verifyHash(password: string, original: string) {
  const originalHash = original.split("$@")[1];
  const salt = original.split("$@")[0];
  const hash = crypto.pbkdf2Sync(password, salt, 2048, 32, "sha512").toString("hex");
  return hash === originalHash;
}

export function generateRoutePathFromFilenameProxy(filename) {
  return (
    "/" +
    filename
      .split("/")
      .reverse()[0]
      .replace(/\.proxy\.(t|j)s/, "")
  );
}

export function createProxy(
  basePath: string,
  targetUrl: string,
  options?: proxy.Config,
) {
  return proxy(`${basePath}/**`, {
    target: targetUrl,
    logLevel: "debug",
    changeOrigin: true,
    prependPath: false,
    pathRewrite: (path, req) => {
      return path.replace(basePath, "");
    },
    ...options,
  });
}

export function onlyNumber(str: string) {
  return str.replace(/[^\d]/g, "");
}

export function generateUuid() {
  return uuid.v4();
}

export function getLogFromReqRes(req) {
  const reqId = generateUuid();
  const timestamp = new Date().toISOString();
  return {
    "@timestamp": timestamp,
    "@Id": reqId,
    query: req.query,
    params: req.params,
    url: req.url,
    fullUrl: req.originalUrl,
    method: req.method,
    headers: req.headers,
    _parsedUrl: req._parsedUrl,
  };
}
