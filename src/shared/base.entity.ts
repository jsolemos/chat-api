import { ApiModelProperty } from "@nestjs/swagger";
import { BaseEntity as TypeOrmBaseEntity, BeforeInsert, Column, PrimaryGeneratedColumn } from "typeorm";

export abstract class BaseEntity extends TypeOrmBaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string | undefined;

  @BeforeInsert()
  updateDate() {
    this.createdAt = new Date();
  }

  @Column({
    type: "timestamp",
    nullable: true,
  })
  @ApiModelProperty({
    format: "date-time",
    type: "string",
    readOnly: true,
  })
  createdAt: Date;

  @Column({
    type: "timestamp",
    nullable: true,
  })
  @ApiModelProperty({
    format: "date-time",
    type: "string",
    readOnly: true,
  })
  updatedAt: Date;

  @Column({
    type: "timestamp",
    nullable: true,
  })
  @ApiModelProperty({
    format: "date-time",
    type: "string",
    readOnly: true,
  })
  deletedAt: Date | null;
}
