import { ClassSerializerInterceptor, MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { APP_FILTER, APP_INTERCEPTOR } from "@nestjs/core";
import { MorganInterceptor, MorganModule } from "nest-morgan";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ConfigModule } from "./config/config.module";
import { AllExceptionsFilter } from "./filters/allExceptions.filter";
import { GlobalInterceptor } from "./interceptors/global.interceptor";
import { AuthModule } from "./modules/auth/auth.module";
import { DatabaseModule } from "./modules/database/database.module";
import { GroupModule } from "./modules/group/group.module";
import { LoggerModule } from "./modules/logger/logger.module";
import { MessageModule } from "./modules/message/message.module";
@Module({
  imports: [ConfigModule, AuthModule, DatabaseModule, LoggerModule, MorganModule, GroupModule, MessageModule],
  exports: [ConfigModule, AuthModule, DatabaseModule, LoggerModule, MorganModule, GroupModule, MessageModule],
  controllers: [AppController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: GlobalInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: MorganInterceptor(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] - :response-time ms ":referrer" ":user-agent"'),
    },
    AppService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void | MiddlewareConsumer {
    // consumer.apply(GlobalMiddleware).forRoutes("**");
  }
}
