import { MigrationInterface, QueryRunner } from "typeorm";
import { Client } from "../modules/clients/models/client.entity";

export class insertCardClient1552682061985 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.connect();
        Client.useConnection(queryRunner.connection);
        await Client.create({
            name: "Card Api",
            email: "cardapi@bbnk.com",
            servicesPermissions: ["all"],
        }).save();
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.connect;
        Client.useConnection(queryRunner.connection);
        await Client.delete({
            email: "cardapi@bbnk.com",
        });
    }

}
