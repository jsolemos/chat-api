import { MigrationInterface, QueryRunner } from "typeorm";
import { Client } from "../modules/clients/models/client.entity";

export class insertBbnkClient1552682076884 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.connect();
        Client.useConnection(queryRunner.connection);
        await Client.create({
            name: "BBNK",
            email: "api@bbnk.com",
            servicesPermissions: ["all"],
        }).save();
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.connect();
        Client.useConnection(queryRunner.connection);
        await Client.delete({
            name: "BBNK",
        });
    }

}
