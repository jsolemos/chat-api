import { LoggerService } from "@nestjs/common";

export class Logger implements LoggerService {
  private readonly logger: any;
  constructor() {
    this.logger = console;
  }

  get stream() {
    return {
      write: (message, encoding) => {
        this.logger.info(message);
      },
    };
  }

  log(...args): void {
    this.logger.log("info", ...args);
  }

  info(...args): void {
    this.logger.info(...args);
  }

  error(...args): void {
    this.logger.error(...args);
  }

  warn(...args) {
    this.logger.warn(...args);
  }
}
