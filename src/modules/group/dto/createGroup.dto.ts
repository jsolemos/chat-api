import { ApiModelProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNotEmpty, ValidateNested } from "class-validator";
import { PhoneDTO } from "../../clients/dto/phone.dto";

export class CreateGroupDTO {
  @ApiModelProperty({
    required: false
  })
  @IsNotEmpty()
  name: string;
  @ApiModelProperty()
  description: string;

  @Type(type => PhoneDTO)
  @ValidateNested()
  @ApiModelProperty()
  participants: PhoneDTO[];

}