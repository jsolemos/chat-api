import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PassportConfig } from "../auth/auth.module";
import { ClientsModule } from "../clients/clients.module";
import { MessageModule } from "../message/message.module";
import { GroupController } from "./group.controller";
import { GroupService } from "./group.service.";
import { Group } from "./models/group.entity";

@Module({
  imports: [
    forwardRef(() => ClientsModule),
    forwardRef(() => MessageModule),
    TypeOrmModule.forFeature([Group]),
    forwardRef(() => PassportConfig),
  ],
  providers: [GroupService],
  exports: [GroupService],
  controllers: [GroupController]
})
export class GroupModule {

}