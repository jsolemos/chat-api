import { Body, Controller, Get, Param, Post, UseGuards } from "@nestjs/common";
import { ApiImplicitParam, ApiUseTags } from "@nestjs/swagger";
import { AuthClient } from "../../decorators/authClient";
import { BaseController } from "../../shared/base.controller";
import { JwtAuthGuard } from "../auth/jwt.auth.guard";
import { MessageDTO } from "../clients/dto/message.dto";
import { PhoneDTO } from "../clients/dto/phone.dto";
import { Client } from "../clients/models/client.entity";
import { CreateGroupDTO } from "./dto/createGroup.dto";
import { GroupService } from "./group.service.";
import { Group } from "./models/group.entity";

@Controller("/groups")
@ApiUseTags("groups")
@UseGuards(JwtAuthGuard)
export class GroupController extends BaseController {
  constructor(private readonly groupService: GroupService) { super(); }
  @Post()
  async create(@Body() _group: CreateGroupDTO, @AuthClient() authClient) {
    const group = Group.create(_group);
    group.owner = authClient;
    group.participants = _group.participants.map(part => Client.create(part));
    return await this.groupService.create(group);
  }

  @Post(":groupId")
  @ApiImplicitParam({
    name: "groupId"
  })
  async invite(@Body() body: PhoneDTO, @Param("groupId") groupId, @AuthClient() authClient) {
    return await this.groupService.addClient(groupId, body.phone, authClient.id);
  }

  @Get(":groupId/participants")
  @ApiImplicitParam({
    name: "groupId"
  })
  async participants(@Param("groupId") groupId, @AuthClient() authClient) {
    return await this.groupService.getParticipants(groupId, authClient.id);
  }

  @Get(":groupId/messages")
  @ApiImplicitParam({
    name: "groupId"
  })
  async messages(@Param("groupId") groupId, @AuthClient() authClient) {
    return await this.groupService.getMessages(groupId, authClient.id);
  }

  @Post(":groupId/messages")
  @ApiImplicitParam({
    name: "groupId"
  })
  async sendMessage(@Body() body: MessageDTO, @Param("groupId") groupId, @AuthClient() authClient) {
    return await this.groupService.sendMessage(groupId, authClient.id, body.text);
  }

  @Get()
  async all(@AuthClient() authClient) {
    return await this.groupService.getGroupsByClientId(authClient.id);
  }
}