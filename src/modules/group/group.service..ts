import { Injectable, UnauthorizedException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as _ from "lodash";
import { Repository } from "typeorm";
import { ClientsService } from "../clients/clients.service";
import { MessageService } from "../message/message.service";
import { Group } from "./models/group.entity";

@Injectable()
export class GroupService {
  constructor(
    private readonly clientService: ClientsService,
    private readonly messageService: MessageService,
    @InjectRepository(Group)
    private readonly groupRepository: Repository<Group>
  ) { }

  async create(group: Group) {
    group.owner = await this.clientService.findOneById(group.owner.id);
    group.participants = (await Promise.all(group.participants
      .map(async (part) => await this.clientService.findOneByPhone(part.phone))))
      .concat(group.owner);
    return await this.groupRepository.save(group);
  }

  async addClient(groupId: string, phone: string, clientId) {
    const group = await this.groupRepository.findOne(groupId, {
      relations: ["owner", "participants"]
    });
    const owner = await group.owner;
    if (owner.id !== clientId) {
      throw new UnauthorizedException("Somente o dono do groupo pode adicionar novos integrantes");
    }
    const client = await this.clientService.findOneByPhone(phone);
    group.participants = _.uniqBy([...((await group.participants) || []), client], "phone");
    return await group.save();
  }

  async getGroupsByClientId(clientId: string) {
    return await this.groupRepository.createQueryBuilder("Group")
      .innerJoin("Group.participants", "participant", "participant.id = :clientId", { clientId })
      .getMany();
  }

  async getParticipants(groupId, clientId) {
    const { participants } = await this.groupRepository.findOne(groupId, {
      relations: ["participants"]
    });

    if (!_.find(participants, {
      id: clientId
    })) throw new UnauthorizedException("Você não participa deste grupo.");

    return await participants;
  }

  async getMessages(groupId, clientId) {
    const { participants } = await this.groupRepository.findOne(groupId, {
      relations: ["participants"]
    });

    if (!_.find(participants, {
      id: clientId
    })) throw new UnauthorizedException("Você não participa deste grupo.");

    return await this.messageService.getMessagesFromGroupId(groupId);

  }

  async sendMessage(groupId, clientId, message: string) {
    const group = await this.groupRepository.findOne(groupId, {
      relations: ["participants"]
    });

    if (!_.find(group.participants, {
      id: clientId
    })) throw new UnauthorizedException("Você não participa deste grupo.");
    const client = await this.clientService.findOneById(clientId);
    return await this.messageService.sendMessagesToGroup(client, group, message);

  }

}