import { ApiModelProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDefined, ValidateNested } from "class-validator";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne } from "typeorm";
import { BaseEntity } from "../../../shared/base.entity";
import { Client } from "../../clients/models/client.entity";
@Entity({
  name: "group"
})
export class Group extends BaseEntity {
  @ApiModelProperty({
    readOnly: true
  })
  @ManyToOne(type => Client)
  owner: Client;

  @ApiModelProperty()
  @Column({
    nullable: false
  })
  name: string;
  @ApiModelProperty({
    required: false
  })
  @Column({
    nullable: false
  })
  description: string;

  @ApiModelProperty({
    required: true,
    isArray: true,
    type: Client
  })
  @IsDefined()
  @ValidateNested()
  @Type(type => Client)
  @ManyToMany(type => Client, {
    cascade: true
  })
  @JoinTable({
    name: "group_client"
  })
  participants: Client[];

}