import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { ConfigService } from "../../config/config.service";
import { ClientsService } from "../clients/clients.service";
import { Client } from "../clients/models/client.entity";
import { JwtPayload } from "./interfaces/jwt.payload";
import { Credentials } from "./models/credentials";

@Injectable()
export class AuthService {
  constructor(
    private readonly clientsService: ClientsService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) { }

  async createToken(credentials: Credentials): Promise<{ expiresIn: string | number, accessToken: string }> {
    // In the real-world app you shouldn't expose this method publicly
    // instead, return a token once you verify user credentials
    const client = await this.clientsService.findByLoginPassword(credentials.email, credentials.password);
    const signOptions = (await this.configService.createJwtOptions()).signOptions;
    return {
      expiresIn: signOptions.expiresIn,
      accessToken: this.jwtService.sign({ client }, signOptions),
    };
  }

  async signIn(credentials: Credentials): Promise<string> {
    // In the real-world app you shouldn't expose this method publicly
    // instead, return a token once you verify user credentials
    const client = await this.clientsService.findByLoginPassword(credentials.email, credentials.password);
    const signOptions = (await this.configService.createJwtOptions()).signOptions;
    return this.jwtService.sign(client, signOptions);
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    return await this.clientsService.findOneById(payload.client.id);
  }

  async register(client: Client) {
    await this.clientsService.create(client);
  }
}