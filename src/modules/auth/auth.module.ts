import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { ConfigModule } from "../../config/config.module";
import { ConfigService } from "../../config/config.service";
import { ClientsModule } from "../clients/clients.module";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { JwtStrategy } from "./jwt.strategy";

export const PassportConfig = PassportModule.register({
  defaultStrategy: "jwt",
  session: false,
  property: "authClient"
});
@Module({
  imports: [
    PassportConfig,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        return configService.createJwtOptions();
      },
      inject: [ConfigService]
    }),
    ClientsModule
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService, JwtStrategy, PassportModule, PassportConfig]
})
export class AuthModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void | MiddlewareConsumer {
    // consumer.apply(JwtAuthMiddleware)
    //   .forRoutes("*");
  }

}
