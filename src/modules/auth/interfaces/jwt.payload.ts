import { Client } from "../../clients/models/client.entity";

export interface JwtPayload {
    client: Client
}