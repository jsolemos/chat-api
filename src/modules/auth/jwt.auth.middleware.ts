import { Inject, MiddlewareFunction, NestMiddleware, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { ConfigService } from "../../config/config.service";
import { Client } from "../clients/models/client.entity";

export class JwtAuthMiddleware implements NestMiddleware {
  constructor(
    @Inject("JwtService") private readonly jwtService: JwtService,
    @Inject("ConfigService") private readonly configService: ConfigService,
  ) { }

  resolve(
    ...args: any[]
  ):
    | MiddlewareFunction<any, any, any>
    | Promise<MiddlewareFunction<any, any, any>> {
    return async (req, res, next) => {
      if (this.configService.getPublicRoutes().includes(req.originalUrl)) {
        return next();
      }
      try {
        console.log(req.headers.authorization);
        const token = (req.headers.authorization || "").split("Bearer ")[1];
        const client = this.jwtService.verify(token);
        req["authClient"] = await Client.findOneOrFail(client.id);
        next();
      } catch (error) {
        console.error({ error });
        if (error.message)
          throw new UnauthorizedException();
      }
    };
  }
}
