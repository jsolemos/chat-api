import { Body, Controller, Post } from "@nestjs/common";
import { ApiUseTags } from "@nestjs/swagger";
import { BaseController } from "../../shared/base.controller";
import { Client } from "../clients/models/client.entity";
import { AuthService } from "./auth.service";
import { Credentials } from "./models/credentials";

@ApiUseTags("auth")
@Controller("/auth")
export class AuthController extends BaseController {
    constructor(private authService: AuthService) {
        super();
    }

    @Post("/login")
    async login(@Body() credentials: Credentials) {
        console.log(credentials);
        return await this.authService.createToken(credentials);
    }

    @Post("/register")
    async register(@Body() client: Client): Promise<boolean> {
        await this.authService.register(client);
        return true;
    }
}
