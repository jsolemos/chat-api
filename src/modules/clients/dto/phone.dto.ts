import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class PhoneDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  phone: string;
}