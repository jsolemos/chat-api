import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class MessageDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  text: string;
}