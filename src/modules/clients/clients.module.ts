import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PassportConfig } from "../auth/auth.module";
import { GroupModule } from "../group/group.module";
import { ClientsController } from "./clients.controller";
import { ClientsService } from "./clients.service";
import { Client } from "./models/client.entity";

@Module({
    imports: [TypeOrmModule.forFeature([Client]), GroupModule, forwardRef(() => PassportConfig)],
    providers: [ClientsService],
    exports: [ClientsService],
    controllers: [ClientsController]
})
export class ClientsModule { }