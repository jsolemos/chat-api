import { ApiModelProperty } from "@nestjs/swagger";
import { Exclude } from "class-transformer";
import { IsEmail } from "class-validator";
import { BeforeInsert, Column, Entity, JoinTable, ManyToMany, Unique } from "typeorm";
import { BaseEntity } from "../../../shared/base.entity";
import { hashPassword } from "../../../shared/utils/helpers";

@Entity({
  name: "client",
})
export class Client extends BaseEntity {

  @ApiModelProperty()
  @Column({
    type: "text",
    nullable: false,
  })
  name: string;

  @ApiModelProperty()
  @Column({
    type: "text",
  })
  @Unique("client_email_uniq", ["email"])
  @IsEmail()
  email: string;

  @ApiModelProperty()
  @Column()
  @Exclude()
  password: string;

  @BeforeInsert()
  encripyPassword() {
    this.password = hashPassword(this.password);
  }

  @ApiModelProperty()
  @Column()
  phone: string;

  @ApiModelProperty({
    readOnly: true
  })
  @ManyToMany(type => Client, {
    lazy: true
  })
  @JoinTable({
    name: "client_friends",
    joinColumn: {
      name: "friendId",
      referencedColumnName: "id"
    },
    inverseJoinColumn: {
      name: "clientId",
      referencedColumnName: "id"
    }
  })
  friends: Client[]
}
