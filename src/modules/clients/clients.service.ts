import { Injectable, NotFoundException, UnauthorizedException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as _ from "lodash";
import { Repository } from "typeorm";
import { onlyNumber, verifyHash } from "../../shared/utils/helpers";
import { MessageService } from "../message/message.service";
import { Message } from "../message/model/message.entity";
import { Client } from "./models/client.entity";
@Injectable()
export class ClientsService {
  constructor(
    private readonly messageService: MessageService,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>
  ) { }

  async findOneById(id) {
    const client = await this.clientRepository.findOne(id);
    if (!client) {
      throw new NotFoundException("Cliente não encontrado.");
    }
    return client;
  }

  async findOneByPhone(phone) {
    const client = await this.clientRepository.findOne({
      where: {
        phone
      }
    });
    if (!client) {
      throw new NotFoundException("Cliente não encontrado.");
    }
    return client;
  }

  async create(client: Client) {
    return await this.clientRepository.save(Client.create(client));
  }

  async getFriendsById(clientId: string) {
    const { friends } = await this.clientRepository.findOne({
      relations: ["friends"],
      where: {
        id: clientId
      }
    });
    return await friends;
  }

  async getChatsByClientId(clientId: string) {
    const messages = await this.clientRepository.manager.createQueryBuilder(Message, "message")
      .leftJoinAndSelect("message.owner", "owner", "message.owner.id = owner.id")
      .leftJoinAndSelect("message.client", "client", "message.client.id = client.id")
      .leftJoinAndSelect("message.group", "group", "message.group.id = group.id")
      .where("message.owner.id = :clientId or message.client.id = :clientId", { clientId })
      .orderBy({
        "message.createdAt": "DESC"
      })
      .getMany();
    return _.uniqBy(_.filter(messages, "group"), "group.id")
      .concat(_.uniqBy(_.filter(messages, "client"), "client.id"));
  }

  async findByLoginPassword(email, password) {
    const client = await Client.findOneOrFail({
      where: {
        email
      }
    });

    if (!verifyHash(password, client.password)) {
      throw new UnauthorizedException("Credenciais inválidas.");
    }
    return client;
  }

  async addFriendFromNumber(phone, clientId) {
    const friend = await this.clientRepository.findOneOrFail({
      where: {
        phone: onlyNumber(phone)
      }
    });

    const client = await this.clientRepository.findOne(clientId, {
      relations: ["friends"]
    });

    console.log(client);
    client.friends = [...(await client.friends || []), friend];
    client.save();
  }

  async getMessages(friendId, clientId) {
    return this.messageService.getMessagesBeteweenClientIdFriendId(clientId, friendId);
  }

  async sendMessage(friendId, clientId, message) {
    const client = await this.clientRepository.findOne(clientId);
    const friend = await this.clientRepository.findOne(friendId);
    return this.messageService.sendMessagesBeteweenClientIdFriendId(client, friend, message);
  }

}