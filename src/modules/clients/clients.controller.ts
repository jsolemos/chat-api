import { Body, Controller, Get, NotFoundException, Param, Post, UseGuards } from "@nestjs/common";
import { ApiImplicitParam, ApiUseTags } from "@nestjs/swagger";
import { AuthClient } from "../../decorators/authClient";
import { BaseController } from "../../shared/base.controller";
import { JwtAuthGuard } from "../auth/jwt.auth.guard";
import { GroupService } from "../group/group.service.";
import { Message } from "../message/model/message.entity";
import { ClientsService } from "./clients.service";
import { MessageDTO } from "./dto/message.dto";
import { PhoneDTO } from "./dto/phone.dto";
import { Client } from "./models/client.entity";

@Controller("clients")
@ApiUseTags("clients")
@UseGuards(JwtAuthGuard)
export class ClientsController extends BaseController {
  constructor(
    private readonly clientsService: ClientsService,
    private readonly groupService: GroupService
  ) { super(); }

  @Get("/me")
  async me(@AuthClient() authClient) {
    return await this.clientsService.findOneById(authClient.id);
  }

  @Get("/friends")
  async fiends(@AuthClient() authClient: Client): Promise<Client[]> {
    return await this.clientsService.getFriendsById(authClient.id);
  }

  private extractInfoByGroupOrMessage(content): {
    targetId: string,
    type: string,
    url: string,
    name: string,
    lastMessage?: string
  } {
    return content.map((item: Message) => {
      const isGroup = !!item.group;
      return {
        targetId: isGroup ? item.group.id : item.client.id,
        type: isGroup ? "group" : "friend",
        url: isGroup ? `/groups/${item.id}/messages` : `/clients/${item.client.id}/messages`,
        name: isGroup ? item.group.name : item.client.name,
        lastMessage: item.text
      };
    });
  }

  @Get("/chats")
  async chats(@AuthClient() authClient: Client): Promise<{ name: string, lastMessage?: string }[]> {
    let chats = [];

    chats = chats.concat(this.extractInfoByGroupOrMessage(await this.clientsService.getChatsByClientId(authClient.id)));
    // chats = chats.concat(this.extractInfoByGroupOrMessage(await this.groupService.getGroupsByClientId(authClient.id)));

    return chats;
  }

  @Post("/friends")
  async addFriends(@Body() body: PhoneDTO, @AuthClient() authClient) {
    try {
      return await this.clientsService.addFriendFromNumber(body.phone, authClient.id);
    } catch (error) {
      throw new NotFoundException("Este contato não é um cliente do chat");
    }
  }

  @Get(":friendId/messages")
  @ApiImplicitParam({
    name: "friendId"
  })
  async messages(@Param("friendId") friendId, @AuthClient() authClient) {
    return await this.clientsService.getMessages(friendId, authClient.id);
  }

  @Post(":friendId/messages")
  @ApiImplicitParam({
    name: "friendId"
  })
  async sendMessage(@Body() body: MessageDTO, @Param("friendId") friendId, @AuthClient() authClient) {
    return await this.clientsService.sendMessage(friendId, authClient.id, body.text);
  }
}
