import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Client } from "../clients/models/client.entity";
import { Group } from "../group/models/group.entity";
import { Message } from "./model/message.entity";

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private readonly messageRepository: Repository<Message>
  ) { }

  async create(message: Message) {
    if (!message.group && message.client)
      throw new BadRequestException("A mesagem deve ser para um grupo ou para um client");

    await this.messageRepository.save(message);
  }

  async getMessagesFromGroupId(groupId: string) {
    return await this.messageRepository.find({
      where: {
        groupId
      },
      order: {
        createdAt: "DESC"
      }
    });
  }

  async getMessagesFromClientId(clientId: string) {
    return await this.messageRepository.find({
      where: {
        clientId
      },
      order: {
        createdAt: "DESC"
      }
    });
  }

  async getMessagesBeteweenClientIdFriendId(clientId: string, friendId: string) {
    return await this.messageRepository.createQueryBuilder("Message")
      .innerJoinAndSelect("Message.owner", "owner", "Message.owner.id = owner.id")
      .innerJoinAndSelect("Message.client", "client", "Message.client.id = client.id")
      .where("Message.client.id = :friendId and Message.owner.id = :clientId", { friendId, clientId })
      .orderBy({
        "Message.createdAt": "DESC"
      })
      .getMany();
  }

  async sendMessagesBeteweenClientIdFriendId(client: Client, friend: Client, message: string) {
    const msg = new Message();
    msg.text = message;
    msg.owner = client;
    msg.client = friend;
    return await msg.save();
  }
  async sendMessagesToGroup(client: Client, group: Group, message: string) {
    const msg = new Message();
    msg.text = message;
    msg.owner = client;
    msg.group = group;
    return await msg.save();
  }
}