import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PassportConfig } from "../auth/auth.module";
import { MessageController } from "./message.controller";
import { MessageService } from "./message.service";
import { Message } from "./model/message.entity";

@Module({
  controllers: [MessageController],
  exports: [MessageService],
  providers: [MessageService],
  imports: [TypeOrmModule.forFeature([Message]), forwardRef(() => PassportConfig)]

})
export class MessageModule {

}