import { Body, Controller, Get, Param, Post, UseGuards } from "@nestjs/common";
import { ApiUseTags } from "@nestjs/swagger";
import { AuthClient } from "../../decorators/authClient";
import { BaseController } from "../../shared/base.controller";
import { JwtAuthGuard } from "../auth/jwt.auth.guard";
import { MessageService } from "./message.service";
import { Message } from "./model/message.entity";

@Controller("messages")
@ApiUseTags("messages")
@UseGuards(JwtAuthGuard)
export class MessageController extends BaseController {
  constructor(
    private readonly messageService: MessageService
  ) { super(); }

  @Post()
  async create(@Body() message: Message, @AuthClient() authClient) {
    message.owner = authClient;
    await this.messageService.create(message);
  }

  @Get("/:clientId")
  async getMessageFromClientId(@AuthClient() authClient, @Param("clientId") clientId) {
    this.messageService.getMessagesFromClientId(clientId);
  }
  @Get("/:groupId")
  async getMessageFromGroupId(@AuthClient() authClient, @Param("groupId") groupId) {
    this.messageService.getMessagesFromGroupId(groupId);
  }
}