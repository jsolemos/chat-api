import { ApiModelProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDefined, IsNotEmpty, ValidateNested } from "class-validator";
import { Column, Entity, ManyToOne } from "typeorm";
import { BaseEntity } from "../../../shared/base.entity";
import { Client } from "../../clients/models/client.entity";
import { Group } from "../../group/models/group.entity";

@Entity({
  name: "message"
})
export class Message extends BaseEntity {

  @ApiModelProperty({
    readOnly: true
  })
  @ManyToOne(type => Client)
  owner: Client;

  @ValidateNested()
  @IsDefined()
  @Type(type => Group)
  @ApiModelProperty()
  @ManyToOne(type => Group, {
    nullable: true,
    eager: true,
  })
  group: Group;

  @ValidateNested()
  @IsDefined()
  @Type(type => Client)
  @ApiModelProperty()
  @ManyToOne(type => Client, {
    nullable: true,
    eager: true,
  })
  client: Client;

  @ApiModelProperty({
    example: {
      name: "imagem.jpeg",
      base64: "aW1hZ2UuanBnYXNkZg=="
    },
    default: "Nome da imagem com extensão, e base64 do buffer",
    isArray: true
  })
  @Column({
    nullable: true,
    array: true,
    type: "json"
  })
  attachements: { name: string, base64: string }[];

  @Column({
    nullable: false,
    type: "text"
  })
  @IsNotEmpty()
  @ApiModelProperty()
  text: string;

}