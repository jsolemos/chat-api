import { BadRequestException, ExecutionContext, Injectable, NestInterceptor, NotFoundException } from "@nestjs/common";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { BusinessException } from "../exceptions/business.exception";
import { ValidationException } from "../exceptions/validation.exception";

@Injectable()
export class GlobalInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, call$: Observable<any>): Observable<any> {
    const req = context.getArgByIndex(0);
    return call$
      .pipe(
        catchError(err => {
          console.error(err);
          return throwError(this.replaceException(err));
        }),
      );
  }

  isPublicException(err) {
    return err instanceof BusinessException
      || err instanceof ValidationException
      || err instanceof NotFoundException;
  }

  replaceException(err) {
    if (this.isPublicException(err)) return err;
    const mapException = {
      EntityNotFound: (msg = "Registro não encontrado.") => new NotFoundException(msg),
      default: () => new BadRequestException()
    };
    const func = mapException[err.name] || mapException["default"];
    return func();
  }
}
