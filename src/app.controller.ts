import { Controller, Get } from "@nestjs/common";
import { ApiOperation } from "@nestjs/swagger";

@Controller()
export class AppController {
    @Get("/check")
    @ApiOperation({ description: "", operationId: "check-api-status", title: "Check api status" })
    check() {
        return { ok: true };
    }
}
