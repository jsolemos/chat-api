import { NestFactory } from "@nestjs/core";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { AppModule } from "./app.module";
import { ValidationPipe } from "./pipes/validation.pipe";

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: console,
  });

  app.useGlobalPipes(new ValidationPipe());

  if (process.env.NODE_ENV === "local")
    app.enableCors();

  const options = new DocumentBuilder()
    .setTitle("Gateway Api")
    .setDescription("The cats API description")
    .setVersion("1.0")
    .addBearerAuth("Authorization", "header")
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("api-docs", app, document);
  await app.listen(process.env.PORT || 8000, "0.0.0.0", () => {
    console.info("Listen in port", process.env.PORT || 8000);
  });
}
bootstrap();
