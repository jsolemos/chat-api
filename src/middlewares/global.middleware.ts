import { Injectable, MiddlewareFunction, NestMiddleware } from "@nestjs/common";
import * as cors from "cors";

@Injectable()
export class GlobalMiddleware implements NestMiddleware {
  resolve(context: string): MiddlewareFunction {
    return cors();
  }
}
