import { MigrationInterface, QueryRunner } from "typeorm";

export class createTableClient1552682052005 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`
        CREATE TABLE "client" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP, "updatedAt" TIMESTAMP, "deletedAt" TIMESTAMP, "name" text NOT NULL, "email" text NOT NULL, "password" character varying NOT NULL, "phone" character varying NOT NULL, CONSTRAINT "client_email_uniq" UNIQUE ("email"), CONSTRAINT "PK_96da49381769303a6515a8785c7" PRIMARY KEY ("id"));
        CREATE TABLE "group" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP, "updatedAt" TIMESTAMP, "deletedAt" TIMESTAMP, "name" character varying NOT NULL, "description" character varying NOT NULL, "ownerId" uuid, CONSTRAINT "PK_256aa0fda9b1de1a73ee0b7106b" PRIMARY KEY ("id"));
        CREATE TABLE "message" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP, "updatedAt" TIMESTAMP, "deletedAt" TIMESTAMP, "attachements" json array, "text" text NOT NULL, "ownerId" uuid, "groupId" uuid, "clientId" uuid, CONSTRAINT "PK_ba01f0a3e0123651915008bc578" PRIMARY KEY ("id"));
        CREATE TABLE "client_friends" ("friendId" uuid NOT NULL, "clientId" uuid NOT NULL, CONSTRAINT "PK_56edb439dc9b461fd0116747e1c" PRIMARY KEY ("friendId", "clientId"));
        CREATE INDEX "IDX_69bdcb1aa9a04cd76ede70952f" ON "client_friends" ("friendId");
        CREATE INDEX "IDX_ff0211fd8c463fe3119351f2f5" ON "client_friends" ("clientId");
        CREATE TABLE "group_client" ("groupId" uuid NOT NULL, "clientId" uuid NOT NULL, CONSTRAINT "PK_2363593e550ae2586b21f4ea55f" PRIMARY KEY ("groupId", "clientId"));
        CREATE INDEX "IDX_093cc052aa815e85fdd0aeb9bd" ON "group_client" ("groupId");
        CREATE INDEX "IDX_4ffe733737f91843cf99e860dc" ON "group_client" ("clientId");
        ALTER TABLE "group" ADD CONSTRAINT "FK_af997e6623c9a0e27c241126988" FOREIGN KEY ("ownerId") REFERENCES "client"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
        ALTER TABLE "message" ADD CONSTRAINT "FK_836fc46066e757709c46e99434e" FOREIGN KEY ("ownerId") REFERENCES "client"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
        ALTER TABLE "message" ADD CONSTRAINT "FK_a85a728f01be8f15f0e52019389" FOREIGN KEY ("groupId") REFERENCES "group"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
        ALTER TABLE "message" ADD CONSTRAINT "FK_f7273f274d3d6ae9c046ab1990b" FOREIGN KEY ("clientId") REFERENCES "client"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
        ALTER TABLE "client_friends" ADD CONSTRAINT "FK_69bdcb1aa9a04cd76ede70952f8" FOREIGN KEY ("friendId") REFERENCES "client"("id") ON DELETE CASCADE ON UPDATE NO ACTION;
        ALTER TABLE "client_friends" ADD CONSTRAINT "FK_ff0211fd8c463fe3119351f2f56" FOREIGN KEY ("clientId") REFERENCES "client"("id") ON DELETE CASCADE ON UPDATE NO ACTION;
        ALTER TABLE "group_client" ADD CONSTRAINT "FK_093cc052aa815e85fdd0aeb9bd2" FOREIGN KEY ("groupId") REFERENCES "group"("id") ON DELETE CASCADE ON UPDATE NO ACTION;
        ALTER TABLE "group_client" ADD CONSTRAINT "FK_4ffe733737f91843cf99e860dc8" FOREIGN KEY ("clientId") REFERENCES "client"("id") ON DELETE CASCADE ON UPDATE NO ACTION;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable("client");
        await queryRunner.dropTable("group");
        await queryRunner.dropTable("message");
        await queryRunner.dropTable("client_friends");
    }

}
