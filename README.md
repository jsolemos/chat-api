# Gateway Api

- [Nestjs](https://docs.nestjs.com)
- [TypeORM](https://typeorm.io/#/)


## Pré requisitos
- Nodejs
- PostgreSQL
  >     $ docker run -d --name postgres -p 5432:5432 postgres:10-alpine

## Instalação

    $ npm i
OBS: renomei o arquivo **envname.env.example** para **dev.env** e ajustes seus valores.

## Executando modo dev
    $ npm run start:dev 

## TYPEORM -  Migrations, Populates, Utils
Caso não tenha executado a aplicação ainda, o arquivo ormconfig.json ainda não foi gerado, ele é necessário para uso via cli, para gera-lo execute:

    $ npm run g:ormconfig # gera arquivo ormconfig.json a partir das suas configurações do arquivo envname.env

---
    # Migrações /src/migrations
    $ npm run migration:generate ExampleMigrate # gera o arquivo de migração
    $ npm run migration:run # executa as migrations
    $ npm run migration:revert # revert última migração executada
---
    # Populate /src/populate
    $ npm run populate:generate ExampleMigrate # gera o arquivo de migração
    $ npm run populate:run # executa as populates
    $ npm run populate:revert # revert último populate executado

Apesar do Nestjs poder sincronizar automaticamente as entidades com o banco de dados,em produção isso pode causar problemas graves, para previnir, iremos trabalhar apenas com migratitons. com o comando abaixo podemos ter a saida da query necessária para geração da migration.

    $ npm run schema:log # revert último populate executado

## Consumir

http:localhost:8000/api-docs
