FROM node:10
WORKDIR /app
ADD package.json package-lock.json /app/
RUN npm ci
ENV TZ America/Sao_Paulo
ADD . /app
CMD ["npm", "run", "start:prod"]
